//! Rename files based on metadata timestamps.
use anyhow::{bail, Context};
use chrono::{DateTime, NaiveDateTime};
use clap::Parser;

mod cli;

fn main() -> anyhow::Result<()> {
    let args = cli::Args::parse();
    let mut arg_files = args.files.clone();

    for file_or_dir in &mut arg_files {
        let file_name = file_or_dir.display().to_string();
        if !file_or_dir.exists() {
            bail!("provided file '{}' cannot be found", file_name);
        }

        if file_or_dir.is_dir() {
            if !args.recursive {
                bail!("cannot process directories without '--recursive' flag");
            }

            for entry in walkdir::WalkDir::new(file_or_dir) {
                let path = entry
                    .with_context(|| format!("failed to scan contents of '{}'", file_name))?
                    .into_path();
                if path.is_dir() {
                    continue;
                }

                rename_file(&path, &args)?;
            }
        } else if file_or_dir.is_file() {
            rename_file(file_or_dir, &args)?;
        } else {
            bail!(
                "argument '{}' is neither file nor directory, aborting",
                file_name
            );
        }
    }

    Ok(())
}

fn rename_file(file: &std::path::Path, args: &cli::Args) -> anyhow::Result<()> {
    let err_context = || format!("failed to rename file '{}'", file.display());
    let source_time_convert =
        |mdt: chrono::ParseResult<NaiveDateTime>| -> anyhow::Result<DateTime<chrono::Utc>> {
            let dt = mdt.context("failed to parse timestamp from filename")?;
            if let Some(input_tz) = args.input_timezone {
                match dt.and_local_timezone(input_tz) {
                    chrono::LocalResult::None => Err(anyhow::anyhow!(
                        "timestamp {} parsed from filename doesn't exist in timezone {}",
                        dt,
                        input_tz
                    )),
                    chrono::LocalResult::Single(val) => Ok(val.with_timezone(&chrono::Utc)),
                    chrono::LocalResult::Ambiguous(val, _) => Ok(val.with_timezone(&chrono::Utc)),
                }
            } else {
                match dt.and_local_timezone(chrono::offset::Local) {
                    chrono::LocalResult::None => Err(anyhow::anyhow!(
                        "timestamp {} parsed from filename doesn't exist in local timezone",
                        dt
                    )),
                    chrono::LocalResult::Single(val) => Ok(val.into()),
                    chrono::LocalResult::Ambiguous(val, _) => Ok(val.into()),
                }
            }
        };

    // Store the timestamp as UTC internally.
    // The input timezone should (in theory) only be relevant when taking the timestamp info from
    // the filename. atime/mtime/ctime store their "timestamp" as EPOCH, which is effectively
    // independent of timezone information.
    let mut datetime: DateTime<chrono::Utc> = match args.datetime_source {
        cli::DatetimeSource::Filename => {
            let file_stem = file
                .file_stem()
                .and_then(|x| x.to_str())
                .context("failed to extract filename for parsing")?;
            let dt: DateTime<chrono::Utc> = DateTime::parse_from_str(file_stem, "%Y%m%dT%H%M%S%Z")
                .or_else(|_| DateTime::parse_from_str(file_stem, "%Y%m%dT%H%M%S%z"))
                .or_else(|_| DateTime::parse_from_str(file_stem, "%Y%m%d %H%M%S%Z"))
                .or_else(|_| DateTime::parse_from_str(file_stem, "%Y%m%d %H%M%S%z"))
                .map(|dt| dt.with_timezone(&chrono::Utc))
                .or_else(|_| {
                    NaiveDateTime::parse_from_str(file_stem, "%Y%m%dT%H%M%SZ")
                        .map(|ndt| ndt.and_utc())
                })
                .or_else(|_| {
                    NaiveDateTime::parse_from_str(file_stem, "%Y%m%d %H%M%SZ")
                        .map(|ndt| ndt.and_utc())
                })
                // Filename pattern from Samsung Smartphone Stock Camera App
                .or_else(|_| {
                    source_time_convert(NaiveDateTime::parse_from_str(file_stem, "%Y%m%d_%H%M%S"))
                })
                // Filename pattern from LineageOS Stock Camera App
                .or_else(|_| {
                    source_time_convert(NaiveDateTime::parse_from_str(
                        file_stem,
                        "%Y-%m-%d-%H-%M-%S-%3f",
                    ))
                })
                .with_context(|| format!("filename to parse was '{file_stem}'"))
                .unwrap();
            dt
        }
        cli::DatetimeSource::Atime => file
            .metadata()
            .and_then(|metadata| metadata.accessed())
            .with_context(err_context)?
            .into(),
        cli::DatetimeSource::Mtime => file
            .metadata()
            .and_then(|metadata| metadata.modified())
            .with_context(err_context)?
            .into(),
        cli::DatetimeSource::Ctime => file
            .metadata()
            .and_then(|metadata| metadata.created())
            .with_context(err_context)?
            .into(),
        cli::DatetimeSource::Now => std::time::SystemTime::now().into(),
    };

    if let Some(ref shift) = args.timeshift {
        let shift = chrono::Duration::seconds(shift.parse().with_context(err_context)?);
        datetime += shift;
    }

    let timestamp_str = match args.output_format {
        cli::OutputFormat::Iso8601 => datetime.format("%Y%m%dT%H%M%SZ"),
        cli::OutputFormat::Ymd => datetime.format("%Y%m%d"),
    };
    let timestamp_filename = if let Some(parent) = file.parent() {
        match parent.canonicalize() {
            Ok(parent_path) => parent_path.join(format!(
                "{}.{}",
                timestamp_str,
                file.extension().unwrap_or_default().to_string_lossy()
            )),
            _ => std::path::PathBuf::from(format!(
                "{}.{}",
                timestamp_str,
                file.extension().unwrap_or_default().to_string_lossy()
            )),
        }
    } else {
        bail!(
            "cannot determine parent of '{}'",
            file.display().to_string()
        );
    };

    std::fs::rename(file, timestamp_filename).with_context(err_context)
}
