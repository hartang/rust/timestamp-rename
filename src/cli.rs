#[derive(Debug, clap::Parser)]
#[command(author, version, about, long_about = None)]
pub struct Args {
    /// Source to obtain date and time from.
    #[arg(long, short = 's', default_value = "now")]
    pub datetime_source: DatetimeSource,

    /// Format to write datetime as.
    #[arg(long, short = 'f', default_value = "ymd")]
    pub output_format: OutputFormat,

    /// Original timezone of sourced datetime.
    #[arg(long, short = 'i')]
    pub input_timezone: Option<chrono_tz::Tz>,

    /// Output timezone to convert sourced datetime into.
    #[arg(long, short = 'o')]
    pub output_timezone: Option<chrono_tz::Tz>,

    /// Apply a timeshift (in seconds) to the sourced datetime.
    #[arg(long)]
    pub timeshift: Option<String>,

    /// Recurse into directories
    #[arg(long, short = 'r')]
    pub recursive: bool,

    /// Files or directories (only if recursive) to process
    pub files: Vec<std::path::PathBuf>,
}

#[derive(Debug, clap::ValueEnum, Clone)]
pub enum DatetimeSource {
    Filename,
    Atime,
    Mtime,
    Ctime,
    Now,
}

#[derive(Debug, Clone, clap::ValueEnum)]
pub enum OutputFormat {
    Iso8601,
    Ymd,
}
